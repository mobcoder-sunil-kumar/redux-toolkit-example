import { createSlice } from '@reduxjs/toolkit'

const initialState = [
  { id: '0', name: 'SUNIL KUMAR YADAV' },
  { id: '1', name: 'SIDDHARTH SINGH' },
  { id: '2', name: 'NIKHIL KUMAR CHAUDHARI' },
]

const usersSlice = createSlice({
    name: 'users',
    initialState,
    reducers:{}
})

export default usersSlice.reducer